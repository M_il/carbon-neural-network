python structure_heatmap.py ../logs/mixed_log/non_relaxed_double0.8_non_relaxed_double0.2/features_few/Rc5/z-score/29-29-29_ba5pct_NEW/struc1/2018-06-11_13.07/ structure_heatmaps_dual_dual/ &>- &
python structure_heatmap.py ../logs/mixed_log/non_relaxed_double0.8_relaxed0.2/features_few/Rc5/z-score/29-29-29_ba5pct_NEW/struc1/2018-06-11_13.07/ structure_heatmaps_dual_relaxed/ &>- &

python structure_heatmap.py ../logs/mixed_log/non_relaxed_single0.8_non_relaxed_single0.2/features_few/Rc5/z-score/29-29-29_ba5pct_NEW/struc1/2018-06-11_13.06/ structure_heatmaps_single_single/ &>- &
python structure_heatmap.py ../logs/mixed_log/non_relaxed_single0.8_relaxed0.2/features_few/Rc5/z-score/29-29-29_ba5pct_NEW/struc1/2018-06-11_13.06/ structure_heatmaps_single_relaxed/ &>- &

python structure_heatmap.py ../logs/mixed_log/multi_perturb/features_few/Rc5/z-score/29-29-29_ba5pct/struc1/2018-06-06_10.13 structure_heatmaps_multi_perturb/ &>- &
